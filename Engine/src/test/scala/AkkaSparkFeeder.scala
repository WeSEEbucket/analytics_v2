package test

import akka.actor._
import com.wesee.analytics.common.{SubscribeReceiver, UnsubscribeReceiver}
import com.wesee.analytics.infra.Impression
import org.apache.spark.SparkConf
import org.apache.spark.util.{LocalSecurityManager, LocalUtils}

import scala.collection.mutable.MutableList
import scala.util.Random

/**
  * Sends the random content to every receiver subscribed with 1/2
  * second delay.
  */
class FeederActor extends Actor with ActorLogging {


  val rand = new Random()
  val listImpression: List[Impression] = List()
  var receivers: MutableList[ActorRef] = new MutableList[ActorRef]()

  def makeMessageBad(): String = {
    "1.5`dp`lax4`509,511,10115,10304`www.livingly.com`appnexus`http://www.livingly.com/trivia/3jNIjkquxyR/1.5`dp`nym2-fe-01`312,429,302,305,490,30124,30125,30155,10207,10304,10156,10306,10004,10272,10142,10053,10099,10208,10325,508`www.msn.com`appnexus`http://www.msn.com/`c``1454891636"
  }

  def makeMessageGood(): String = {
    "1.5`dp`nym2-fe-01`312,429,302,305,490,30124,30125,30155,10207,10304,10156,10306,10004,10272,10142,10053,10099,10208,10325,508`www.msn.com`appnexus`http://www.msn.com/`c``1454891636"
  }
  /*
   * A thread to generate random messages
   */
  new Thread() {

    override def run() {
      while (true) {
        Thread.sleep(20000)
        if (receivers.length > 0) {
          var msg = makeMessageBad
          receivers.foreach(_ ! msg)
          msg = makeMessageGood
          receivers.foreach(_ ! msg)
          msg = makeMessageGood
          receivers.foreach(_ ! msg)
        }
      }
    }
  }.start()

  def receive: Receive = {

    case SubscribeReceiver(receiverActor: ActorRef) =>
      println("received subscribe from %s".format(receiverActor.toString))
      receivers = MutableList(receiverActor) ++ receivers

    case UnsubscribeReceiver(receiverActor: ActorRef) =>
      println("received unsubscribe from %s".format(receiverActor.toString))
      receivers = receivers.dropWhile(x => x eq receiverActor)

  }
}

/**
  * A sample feeder actor
  *
  * Usage: FeederActor <hostname> <port>
  * <hostname> and <port> describe the AkkaSystem that Spark Sample feeder would start on.
  */
object FeederActor {

  def main(args: Array[String]) {
    if (args.length < 2) {
      System.err.println("Usage: FeederActor <hostname> <port>\n")
      System.exit(1)
    }
    val Seq(host, port) = args.toSeq

    val config = new SparkConf

    val actorSystem = LocalUtils.obj.createActorSystem("AnalyticDpStreamer", host, port.toInt, config,
      LocalSecurityManager.get(config))._1

    val feeder = actorSystem.actorOf(Props[FeederActor], "FeederActor")

    println("Feeder started as:" + feeder)

    actorSystem.awaitTermination()

  }
}