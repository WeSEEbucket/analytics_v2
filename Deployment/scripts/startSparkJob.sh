#!/usr/bin/env bash

SPARK_HOME="/opt/deploy/spark"

ANALYTICES_HOME="/home/analytics"

#SPARK_MASTER=local[*]
SPARK_MASTER=spark://`hostname`:7077

JAR=$ANALYTICES_HOME/bin/Engine.jar

CLASS=com.wesee.analytics.spark.report.SparkDfQueryApp

SPARK_DEPLOY_MODE="cluster"

JAVA_OPTS="-Dspark.local.dir=$ANALYTICES_HOME/temp
           -Djava.net.preferIPv4Stack=true \
           -Djava.library.path=/usr/lib64/libsnappy.so \
           -Dorg.xerial.snappy.tempdir=/home/analytics/temp"

JAVA_LOGS="-Dlog4j.configuration=file:$ANALYTICES_HOME/conf/log4j.sparkJob.properties"

JAVA_CONF="-Dconfig.file=$ANALYTICES_HOME/conf/sparkApplication.conf"

#ARGS1="ALL"
#ARGS2="select * from impressions limit 1"
#ARGS3="/2015/12/03/~/2015/12/07/"
#ARGS4="/yyyy/MM/dd/"

exec "$SPARK_HOME"/bin/spark-submit \
  --master $SPARK_MASTER \
  --class $CLASS \
  --driver-java-options "$JAVA_OPTS $JAVA_LOGS $JAVA_CONF" \
  --deploy-mode $SPARK_DEPLOY_MODE \
  "$JAR" \
  "$@"