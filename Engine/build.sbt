name := "Engine"

unmanagedJars in Compile += file("../Common/target/scala-2.11/Common.jar")

assemblyJarName in assembly := "Engine.jar"

mainClass in assembly := Some("com.wesee.analytics.spark.stream.SparkStreamReceiver")

def sparkVersion = "1.6.0"
def akkaVersion = "2.3.11"

libraryDependencies ++= Seq(
  "io.netty" % "netty-all" % "5.0.0.Alpha2",
  "io.netty" % "netty" % "3.10.5.Final",
  "org.apache.spark" % "spark-core_2.11" % sparkVersion,
  "org.apache.spark" % "spark-streaming_2.11" % sparkVersion,
  "org.apache.spark" % "spark-sql_2.11" % sparkVersion,
  "com.databricks" % "spark-csv_2.11" % "1.3.0",
  "com.typesafe" % "config" % "latest.integration",
  "com.typesafe.akka" % "akka-actor_2.11" % akkaVersion,
  "com.typesafe.akka" % "akka-remote_2.11" % akkaVersion,
  "com.typesafe.akka" % "akka-slf4j_2.11" % akkaVersion,
  "org.xerial.snappy" % "snappy-java" % "1.1.2",
  "joda-time" % "joda-time" % "2.8.2"//,
)

assemblyMergeStrategy in assembly := {

  case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
  case PathList("javax", "activation", xs@_*) => MergeStrategy.first
  case PathList("org", "apache", xs@_*) => MergeStrategy.first
  case PathList("org", "joda", xs@_*) => MergeStrategy.first
  case PathList("org", "jboss", xs@_*) => MergeStrategy.first
  case PathList("org", "xerial", xs@_*) => MergeStrategy.first
  case PathList("com", "google", xs@_*) => MergeStrategy.first
  case PathList("com", "fasterxml", xs@_*) => MergeStrategy.first
  case PathList("com", "typesafe", xs@_*) => MergeStrategy.first
  case PathList("xml-apis", "xml-apis", xs@_*) => MergeStrategy.first
  case PathList("com", "esotericsoftware", xs@_*) => MergeStrategy.first
  case PathList("javax", "xml", xs@_*) => MergeStrategy.first
  case PathList("org", "slf4j", xs@_*) => MergeStrategy.first
  case PathList("scala", "reflect", xs@_*) => MergeStrategy.first
  case PathList("log4j", "log4j", xs@_*) => MergeStrategy.first
  case PathList("commons-beanutils", "commons-beanutils-core", xs@_*) => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
