package com.wesee.analytics.spark.report

import com.wesee.analytics.infra.{FilesUtils, Logging, SparkConfig}
import org.apache.spark.sql.{SaveMode}


/**
Created by raanan on 16/11/15.

  arg(0) = report file name
  arg(1) = data format (parquet ...)
  arg(2) = date range { From date to date. format_from  ~  format_to}
  arg(3) = date format { like yyyy/MM/dd }
  * */

object SparkCmdQueryApp extends App with SparkConfig with Logging {

  // Validate input arguments
  (args.length == 4) match {
    case true => None
    case false => log.error("SparkDfQueryApp:  Expecting input arguments: [report name] [data format] [from date ~ to data] [date format]")
      log.error("Terminated SparkDfQueryApp !")
      System.exit(1)
  }

  val reportName = args(0)
  log.info(s"Started SparkDfQueryApp for report : $reportName with input argumnets " + args.deep.mkString(" "))

  val dataFormat = args(1)
  log.info(s"Report Data format : $dataFormat")

  val dateRange = args(2)
  log.info(s"Report Date Range : $dateRange")

  val dateFormat = args(3)
  log.info(s"Report Date Format : $dateFormat")

  // Get report data
  val impressionsDF = ReportsUtils.getDataFrame(dataFormat, dateRange, dateFormat)

  // process report data
  val reportDF = ReportsCommands.getDataFrame(reportName, impressionsDF)

  // Save processed data to report file
  reportDF.coalesce(reportPartitions)
    .write
    .format(reportFileFormat)
    .option("header", "true")
    .mode(SaveMode.Overwrite)
    .save(FilesUtils.generateReportFileName(reportBaseDirectory,
      reportDirectoryDatePattern,
      reportName,
      reportFileDatePattern))

  log.info(s"Finished SparkDfQueryApp for report : $reportName")
}