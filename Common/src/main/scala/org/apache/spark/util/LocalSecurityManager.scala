package org.apache.spark.util

import org.apache.spark.{SecurityManager, SparkConf}

object LocalSecurityManager {
  def get(config: SparkConf): org.apache.spark.SecurityManager = {
    new SecurityManager(config)
  }
}