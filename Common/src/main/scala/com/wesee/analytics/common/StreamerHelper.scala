package com.wesee.analytics.common

import akka.actor.ActorRef

case class SubscribeReceiver(receiverActor: ActorRef)

case class UnsubscribeReceiver(receiverActor: ActorRef)

case class Dispatch(impression: String)