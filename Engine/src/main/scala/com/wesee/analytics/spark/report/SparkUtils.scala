package com.wesee.analytics.spark.report

import com.wesee.analytics.infra.SparkConfig
import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by raanan on 12/14/15.
  *
  */
object SparkUtils extends SparkConfig {

  def getSQLContext(): SQLContext = {

    val sparkConf = new SparkConf().setAppName(reportAppName).setMaster(sparkMaster)
    val sparkContext = SparkContext.getOrCreate(sparkConf)
    val sqlContext = SQLContext.getOrCreate(sparkContext)

    return sqlContext
  }


}
