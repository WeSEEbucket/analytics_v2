package com.wesee.analytics.spark.report

import com.wesee.analytics.infra.{FilesUtils, Logging, SparkConfig}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SaveMode
import org.joda.time.{DateTime, DateTimeZone}

object SparkSqlQueryHiveApp extends App with SparkConfig with Logging {
/*
  // Validate input arguments
  (args.length == 2) match {
    case true => None
    case false => log.error("Expecting input arguments: [report name] [sql command]")
      args.foreach(arg => println(arg))
      println (args.length)
      System.exit(1)
  }

  val reportName = args(0)
  log.info(s"Started SparkSqlQueryHiveApp for report : $reportName")

  val query = args(1)
  log.info(s"Pre Format SQL command : $query")

  val conf = new SparkConf()

  val date = DateTime.now(DateTimeZone.UTC).minusDays(1)

  var qeury = query.format(
    date.getYear,
    date.getMonthOfYear,
    date.getDayOfMonth)

  log.info(s"Format SQL command : $query")


  sqlContext.sql(qeury).write.
    format("com.databricks.spark.csv").
    mode(SaveMode.Overwrite).
    option("header", "true").
    save(FilesUtils.generateReportFileName(
      reportBaseDirectory,
      reportFileDatePattern,
      reportName)
  )

  */
}