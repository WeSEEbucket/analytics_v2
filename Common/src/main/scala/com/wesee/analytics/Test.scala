package com.wesee.analytics

import com.wesee.analytics.common._
import com.wesee.analytics.infra.MonitorConfig

object Test extends App with MonitorConfig {

  println("start")

  MonitorManager.increment("test", 8888)

  println("end")


}
