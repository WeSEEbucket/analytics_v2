package com.wesee.analytics.infra

import akka.actor.ActorSystem

object ImpressionParser extends Logging {

  implicit var system: ActorSystem = null

  def apply(_system: ActorSystem) {
    system = _system
  }

  def parse(input: String): Option[Impression] = {
    try {

      val properties = input.split('`')

      val classificationsList: List[Int] = properties(ImpressionProperties.classifications) match {
        case "" => List[Int]()
        case values: String => values.split(",").toList.map(_.toInt)(collection.breakOut): List[Int]
      }

      val classificationsMap: Map[Int, Int] = properties(ImpressionProperties.classifications) match {
        case "" => Map[Int, Int]()
        case values: String => values.split(",").map(t => t.toInt -> t.toInt)(collection.breakOut): Map[Int, Int]
      }

      Some(Impression(
        properties(ImpressionProperties.version),
        properties(ImpressionProperties.callerType),
        properties(ImpressionProperties.identification),
        classificationsMap,
        classificationsList,
        properties(ImpressionProperties.authority),
        properties(ImpressionProperties.client),
        properties(ImpressionProperties.location),
        properties(ImpressionProperties.cacheHit),
        properties(ImpressionProperties.queryParams),
        properties(ImpressionProperties.time).toLong))
    }
    catch {
      case unknown: Throwable =>
        log.error(s"Ignoring this impression, Got this unknown exception: $unknown || Input : $input ", unknown)
        None
    }
  }
}