package com.wesee.analytics.spark.stream.recievers

import akka.actor._
import com.wesee.analytics.common.{UnsubscribeReceiver, SubscribeReceiver}
import com.wesee.analytics.infra.{ImpressionParser, streamingOutConfig}
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.receiver.ActorHelper

/**
  * A sample actor as receiver, is also simplest. This receiver actor
  * goes and subscribe to a typical publisher/feeder actor and receives
  * data.
  *
  */
class ImpressionActorReceiver[T]()
  extends Actor with ActorHelper with streamingOutConfig {

  private val urlOfPublisher = s"$connectionProtocol://$actorSystemName@$listenHostname:$listenPort/user/$actorName"

  log.info(s"Streamer url Of publisher : $urlOfPublisher")

  private val remotePublisher = context.actorSelection(urlOfPublisher)

  override def preStart(): Unit =  {
      remotePublisher ! SubscribeReceiver(context.self)
    }

  def receive = {
    case msg: String => ImpressionParser
      .parse(msg.asInstanceOf[String])
      .foreach(store(_))
  }

  override def postStop(): Unit = remotePublisher ! UnsubscribeReceiver(context.self)
}