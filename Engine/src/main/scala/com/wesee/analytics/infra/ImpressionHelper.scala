package com.wesee.analytics.infra

import akka.actor.ActorRef


case class Classification(id: Int)

case class Impression(version: String,
                      callerType: String,
                      identification: String,
                      classificationsMap: Map[Int, Int],
                      classificationsList: List[Int],
                      authority: String,
                      client: String,
                      location: String,
                      cacheHit: String,
                      queryParams: String,
                      time: Long)

object ImpressionProperties extends Enumeration {
  type Index = Value
  val version = 0
  val callerType = 1
  val identification = 2
  val classifications = 3
  val authority = 4
  val client = 5
  val location = 6
  val cacheHit = 7
  val queryParams = 8
  val time = 9
}
