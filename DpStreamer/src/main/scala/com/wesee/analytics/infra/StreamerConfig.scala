package com.wesee.analytics.infra

trait streamingInConfig extends Config {

  private val streamingInConfig = config.getConfig("streamingIn")

  val actorSystemName = streamingInConfig.getString("actor-system-name")
  val listenHostname = streamingInConfig.getString("host")
  val listenPort = streamingInConfig.getInt("port")

}