package com.wesee.analytics.spark.stream.recievers

import akka.actor.{Actor, Props}
import com.wesee.analytics.common.{SubscribeReceiver, UnsubscribeReceiver}
import com.wesee.analytics.infra._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SQLContext
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.receiver.ActorHelper

/**
 * A sample actor as receiver, is also simplest. This receiver actor
 * goes and subscribe to a typical publisher/feeder actor and receives
 * data.
 *
 */
class GenericActorReceiver(schemaType : String) // Schema Type = "parquet-flat" | "parquet-compound" |"parquet-semicompound" | "text"
  extends Actor with ActorHelper with streamingOutConfig {

  val reciever = Reciever(schemaType)

  private val urlOfPublisher = s"$connectionProtocol://$actorSystemName@$listenHostname:$listenPort/user/$actorName"

  log.info(s"$schemaType Streamer url Of publisher : $urlOfPublisher")

  private val remotePublisher = context.actorSelection(urlOfPublisher)

  override def preStart(): Unit = remotePublisher ! SubscribeReceiver(context.self)

  def receive: PartialFunction[Any, Unit] = {
    case msg:String => reciever.store(this, msg)
  }

  override def postStop(): Unit = remotePublisher ! UnsubscribeReceiver(context.self)

  // Todo Add reconnect case handling
}


trait Reciever
{
  def store(reciever :GenericActorReceiver, msg : String)
}

object Reciever {

  private class parquetFlatReciever extends Reciever {
    override def store(reciever :GenericActorReceiver,impression : String) { }
  }

  private class parquetCompoundReciever extends Reciever {
    override def store(reciever :GenericActorReceiver,impression : String)  { }
  }

  private class parquetSemiCompoundReciever extends Reciever {
    override def store(reciever :GenericActorReceiver,impression : String)  {
      reciever.store(ImpressionParser.parse(impression))  }
  }

  private class textReciever extends Reciever {
    override def store(reciever :GenericActorReceiver,impression : String)  { reciever.store(impression) }
  }

  // the factory method
  def apply(recieverType: String): Reciever = {
    recieverType match
    {
      case "parquet-flat" => new parquetFlatReciever
      case "parquet-compound" => new parquetCompoundReciever
      case "parquet-semicompound" => new parquetSemiCompoundReciever
      case _ => new textReciever
    }
  }
}


object StreamingContextFactory {
  def apply(recieverType: String , ssc: StreamingContext, receiverActorName : String) : DStream[_] = {
    recieverType match
    {
      case "parquet-flat" => null
      case "parquet-compound" => null
      case "parquet-semicompound" => ssc.actorStream[Impression](Props(new GenericActorReceiver(recieverType)), receiverActorName)
      case _ => ssc.actorStream[String](Props(new GenericActorReceiver(recieverType)), receiverActorName)
    }
  }

  import scala.reflect.{ClassTag, classTag}
  def matchRDD[A : ClassTag](rdd: RDD[A]) =
    {
      // Get the singleton instance of SqlContext
      val sqlContext = SQLContext.getOrCreate(rdd.sparkContext)
      import sqlContext.implicits._

      if (classTag[A] == classTag[Impression])
        println("classTag : " + classTag[A].getClass)
      rdd match {
      case impressionRDD: RDD[Impression] if classTag[A] == classTag[Impression] =>
        {
          println("impression_kuku")
          rdd.asInstanceOf[RDD[Impression]].toDF()
        }

      case strRDD: RDD[String] if classTag[A] == classTag[String] =>
        {
          println("string_kuku")
          rdd.asInstanceOf[RDD[String]].toDF()
        }

    }
  }
}

