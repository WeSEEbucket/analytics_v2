package com.wesee.analytics.spark.report

import com.wesee.analytics.infra.{FilesUtils, Logging, SparkConfig}
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.DataFrame

import scala.collection.mutable.ArrayBuffer

/**

arg(0) = report file name
  arg(1) = query { SQL command }
  arg(2) = range { From date to date. format_from  ~  format_to}
  arg(3) = date format { like yyyy/MM/dd }
  arg(4) = data format = "parquet" | "text" | "orc" \ "json"
  arg(5) = Schema Type = "parquet-flat" | "parquet-compound" | parquet-semicompound | "text"

  * */

object SparkQueryAppText extends App with SparkConfig with Logging {

  // Validate input arguments
  (args.length == 4) match {
    case true => None
    case false => log.error("Expecting input arguments: [report name] [from date ~ to data] [date format] [schema type] ")
      args.foreach(arg => println(arg))
      println(args.length)
      System.exit(1)
  }

  val reportName = args(0)

  log.info(s"Started SparkQueryAppText for report : $reportName")
  log.info(args.deep.mkString(" "))

  // Get date range & date format
  val reportDateRange = args(1)
  log.info(s"Report Date Range : $reportDateRange")

  val reportDateFormat = args(2)
  log.info(s"Report Date Format : $reportDateFormat")

  val schemaType = args(3)
  log.info(s"Data Schema Type : $schemaType")

  val conf = new SparkConf().setAppName(reportName).setMaster(sparkMaster)

  val sc = new SparkContext(conf)

  // Get the files to process for that date range
  var files: Seq[String] = FilesUtils.getDirectoriesForDateRange(dataBaseDirectory, schemaType, reportDateRange, reportDateFormat)

  log.debug("Files : " + files.mkString)

  // Load files and transform to DataFrames
  val sqlContext = SparkUtils.getSQLContext()

  val dfs = files.map( file => {sqlContext.read.format("com.databricks.spark.csv").option("delimiter",dataTextDelimiter).option("header", "false").load(file)} )

  val mdfs = dfs.reduce((x,y) => x.unionAll(y))

  def func(row:org.apache.spark.sql.Row):Array[String] = {
    if (row.get(0) != null) row.get(0).toString.split(',')
    else Array()
  }

  mdfs.select(mdfs.col("C1")).rdd.
    flatMap(row => func(row)).
    map( id => (id,1)).
    reduceByKey(_ + _).
    sortBy(_._2, false).coalesce(reportPartitions).
    saveAsTextFile(FilesUtils.generateReportFileName(reportBaseDirectory,
      reportDirectoryDatePattern,
      reportName,
      reportFileDatePattern))

  log.info(s"Finished SparkQueryAppText for report : $reportName")

}