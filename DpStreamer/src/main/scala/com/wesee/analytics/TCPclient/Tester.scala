package com.wesee.analytics.TCPclient

/*
import java.net.InetSocketAddress

import com.wesee.analytics.infra.streamingInConfig

import akka.actor._
import akka.io.{IO, Tcp}
import akka.util.ByteString

object Tester extends App with streamingInConfig {

  implicit val system = ActorSystem(actorSystemName)

  val endpoint = new InetSocketAddress(akkaRemoteListenHostname, akkaRemoteListenPort)

  println(s"Connecting to : $endpoint")
  val actor = system.actorOf(Client.props(endpoint), "AnalyticsDPTester")

  Thread.sleep(1000)
  println(s"sending 'ping' to actor($actor)")
  actor ! "ping"
}


object Client {
  def props(remote: InetSocketAddress) =
    Props(classOf[Client], remote)
}

class Client(remote: InetSocketAddress) extends Actor {
  var hammeringCount : Int = 1
  var repeatCount : Int = 10
  //var variance : Double

  IO(Tcp) ! Connect(remote)

  def receive = {
    case CommandFailed(_: Connect) =>
      //listener ! "connect failed"
      context stop self

    case c @ Connected(remote, local) =>
      //listener ! c
      val connection = sender()
      println(s"c($c) remote($remote), local($local)")
      connection ! Register(self)
      context become {
        case CommandFailed(w: Write) =>
          // O/S buffer was full
          println("write failed")
          self ! "write failed"
        case Received(data) =>
          val text = data.utf8String.trim
          //println(s"Received $text from remote address $remote")
          text match {
            case "pong" =>
              println("got pong")
              hammer(connection,"{\"response\":[{\"id\":101,\"IAB\":\"\"}],\"authority\":\"ilan-zucker.net76.net\",\"time\":1449731658,\"queryParams\":\"[com.wesee.adp.httpurl.HttpUrl$Param@204bb87]\",\"client\":\"\",\"location\":\"http:\\/\\/ilan",hammeringCount)
              Thread.sleep(-1)
              //hammer(connection,"-zucker.net76.net\\/?abc=gil0076\",\"cacheHit\":\"\",\"version\":\"1.0\"}\r",hammeringCount)
              //hammer(connection,"{\"response\":[{\"id\":101,\"IAB\":\"\"}],\"authority\":\"ilan-zucker.net\",\"time\":1449731658,\"queryParams\":\"[com.wesee.adp.httpurl.HttpUrl$Param@204bb87]\",\"client\":\"\",\"location\":\"http:\\/\\/ilan-zucker.net76.net\\/?abc=gil0076\",\"cacheHit\":\"\",\"version\":\"1.0\"}\r",2)
              //println("sending ping")
              connection ! Write(ByteString("ping"+"\r"))
          }
        case "ping" =>
          connection ! Write(ByteString("ping"+"\r"))
        case "close" =>
          connection ! Close
          //EndService(true)
        case _: ConnectionClosed =>
          println("connection closed")
          context stop self
          //EndService(true)
        case _ =>
          println("unhandled")
      }
    case _ =>
      println("unhandled")
  }

  def hammer(sender :ActorRef , data : String, times : Int) =
  {
    for(a <- 1 to times)
      sender ! Write(ByteString(data))
  }
}
*/