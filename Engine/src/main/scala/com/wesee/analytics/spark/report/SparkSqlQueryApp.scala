package com.wesee.analytics.spark.report

import com.wesee.analytics.infra.{FilesUtils, Logging, SparkConfig}
import org.apache.spark.sql.{DataFrame, SaveMode}

import scala.collection.mutable.ArrayBuffer

/**

  arg(0) = report file name
  arg(1) = query { SQL command }
  arg(2) = range { From date to date. format_from  ~  format_to}
  arg(3) = date format { like yyyy/MM/dd }
  arg(4) = data format = "parquet" | "text" | "orc" \ "json"
  arg(5) = Schema Type = "parquet-flat" | "parquet-compound" | parquet-semicompound | "text"


  * */

object SparkSqlQueryApp extends App with SparkConfig with Logging {

  // Validate input arguments
  (args.length == 6) match {
    case true => None
    case false => log.error("Expecting input arguments: [report name] [SQL command] [from date ~ to data] [date format] [data format] [schema type] ")
      args.foreach(arg => println(arg))
      println (args.length)
      System.exit(1)
  }

  val reportName = args(0)

  log.info(s"Started SparkSqlQueryApp for report : $reportName")
  log.info(args.deep.mkString(" "))

  // Get SQL command
  val sqlCommand = getSqlCommand(args(1))
  log.info(s"SQl Command : $sqlCommand")

  // Get date range & date format
  val reportDateRange = args(2)
  log.info(s"Report Date Range : $reportDateRange")

  val reportDateFormat = args(3)
  log.info(s"Report Date Format : $reportDateFormat")

  val dataFormat = args(4)
  log.info(s"Report Input Data Format : $dataFormat")

  val schemaType = args(5)
  log.info(s"Data Schema Type : $schemaType")


  log.info(FilesUtils.generateReportFileName(reportBaseDirectory,
    reportDirectoryDatePattern,
    reportName,
    reportFileDatePattern))

  // Get the files to process for that date range
  var files: Seq[String] = FilesUtils.getDirectoriesForDateRange(dataBaseDirectory, schemaType, reportDateRange, reportDateFormat)

  log.debug(s"Files : $files")

  // Load files and transform to DataFrames
  val sqlContext = SparkUtils.getSQLContext()

  val rddParquet: Seq[DataFrame] = files.map(file => sqlContext.read.format(dataFormat).load(file))

  // Join DataFrames to one DataFrame
  var impressions: DataFrame = rddParquet.reduce((s1, s2) => s1.unionAll(s2))

  // Create Spark SQL interface
  impressions.registerTempTable(sparkSchemaName)

  // Execute the SQL command
  val countQuery = sqlContext.sql(sqlCommand)

  // Collect data and save report as (CSV)
  countQuery.coalesce(reportPartitions).write
    .format(reportFileFormat)
    .option("header", "true")
    .mode(SaveMode.Overwrite)
    .save(FilesUtils.generateReportFileName(reportBaseDirectory,
      reportDirectoryDatePattern,
      reportName,
      reportFileDatePattern))

  def getSqlCommand(sqlCommand: String): String = {
    sqlCommand
  }

  log.info(s"Finished SparkSqlQueryApp for report : $reportName")
}