package com.wesee.analytics.spark.stream.apps

import com.wesee.analytics.common._
import com.wesee.analytics.infra.{FilesUtils, Logging, SparkConfig}
import com.wesee.analytics.spark.stream.recievers.StreamingContextFactory
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

// INPUT ARGUMENTS
// 1.  Schema Type = "parquet-flat" | "parquet-compound" | parquet-semicompound | "text"
// 2.  Write format = "parquet" | "text" | "orc" \ "json"

object GenericSparkStreamReceiver extends App with SparkConfig with Logging {

  // Validate input arguments
  (args.length == 2) match {

    case true => None
    case false => log.error("GenericSparkStreamReceiver: Expecting input arguments: [schema type] [output format]")
      log.error("Terminated GenericSparkStreamReceiver !")
      System.exit(1)
  }

  val schemaType = args(0)
  val writeFormat = args(1)

  log.info(s"Started Spark Stream Application for")
  log.info(s"Schema Type : '$schemaType'")
  log.info(s"Data format : '$writeFormat'")
  log.info(s"Data directory date pattern : '$dataDirectoryDatePattern'")

  val appName = schemaType + "_" + writeFormat

  val sparkConf = new SparkConf()
    .setAppName(appName)
    .setMaster(sparkMaster)
    .set("spark.akka.logLifecycleEvents", "false") // not redirect undelivered events to dead letter channel

  // Create the context and set the batch size
  val ssc = new StreamingContext(sparkConf, Seconds(sparkStream_BatchDuration))

  /*
   * Following is the use of actorStream to plug in custom actor as receiver
   */

  val impressions = StreamingContextFactory(schemaType,ssc,receiverActorName)//ssc.actorStream[Impression](Props(new GenericActorReceiver(schemaType)), receiverActorName)

  impressions.foreachRDD { rdd =>

    MonitorManager.increment(appName + "_Total_Rounds", 1L)

    // Convert RDD[String] to DataFrame
    var impressionsDataFrame = StreamingContextFactory.matchRDD(rdd)

    // Set data partition in (# of files in filesystem) if value set (!=-1)
    dataPartitions match {
      case -1 => None
      case _ => impressionsDataFrame = impressionsDataFrame.coalesce(dataPartitions)
    }

    log.isDebugEnabled match {
      case true => impressionsDataFrame.foreach(x => log.debug(x.toString()))
      case false => None
    }

    try {

      impressionsDataFrame.count() match {

        case 0L => log.info("RDD is empty, not writing output")

          MonitorManager.set(appName + "_Last_WrittenRecords", 0L)
          MonitorManager.increment(appName + "_Total_WrittenRecords", 0L)

        case counter: Long => log.info(s"Number of Records  $counter.toString")

          impressionsDataFrame.write.format(writeFormat).save(FilesUtils.generateDataFileName(
            dataBaseDirectory,
            schemaType,
            dataDirectoryDatePattern
            ))

          MonitorManager.set(appName + "_Last_WrittenRecords", counter)
          MonitorManager.increment(appName + "_Total_WrittenRecords", counter)
      }

    } catch {

      case exception: Throwable => log.error(s"Got Throwable execption when trying writing output$exception.getMessage  $exception.printStackTrace()")
        MonitorManager.increment(appName + "_Total_Exceptions", 1L)
    }
  }

  ssc.start()
  ssc.awaitTermination()

  log.info(s"Terminated  $appName")
}