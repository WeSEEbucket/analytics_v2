package com.wesee.analytics.infra

import scala.sys.process.{ProcessLogger, Process}


object Utils extends Logging {

  private def executeCommand(cmd: String): (List[String], List[String], Int) = {

    val qb = Process(cmd)
    var out = List[String]()
    var err = List[String]()

    val exit = qb ! ProcessLogger((s) => out ::= s,
      (s) => err ::= s)

    (out.reverse, err.reverse, exit)
  }

  def Stop(stopCmd: String): Boolean = {

    var returnValue: Boolean = false;
    val (out, err, exitCode) = executeCommand(stopCmd)

    if (!err.isEmpty)
      log.error(s"Error while STOPPING $stopCmd with error ", err)

    exitCode match {
      case 0 => false
      case 1 => returnValue = true
      case _ => log.info(s"Unexpected exitCode : +${err.toString()}", out)
    }

    (returnValue)
  }


}
