package com.wesee.analytics.streamer

import akka.actor._
import akka.io.Tcp.{Write, ConnectionClosed, Connect, CommandFailed}
import com.wesee.analytics.common.{Dispatch, AggregatorMonitorManager, SubscribeReceiver , UnsubscribeReceiver}
import com.wesee.analytics.infra.{Logging, streamingOutConfig}
import org.apache.spark.SparkConf
import org.apache.spark.util.{LocalSecurityManager, LocalUtils}

import scala.collection.mutable.MutableList


class FeederActor extends Actor with ActorLogging {
  var receivers: Option[MutableList[ActorRef]] = Option(new MutableList[ActorRef]())

  def send(receiver: ActorRef, impression: String) = {
    receiver ! impression
    log.debug(s"${impression} was sent to $receiver")
    AggregatorMonitorManager.increment("DpStreamerOut_Total_Impressions_" + self, 1L)
  }

  def receive: Receive = {

    case Dispatch(impression: String) => {
      receivers.get.foreach(send(_, impression))
    }

    case SubscribeReceiver(receiverActor: ActorRef) => {
      context.watch(receiverActor)
      receivers = Option(MutableList(receiverActor) ++ receivers.get)
      log.info(s"received subscribe from $receiverActor , total number of recievers : ${receivers.get.length}")
    }

    case UnsubscribeReceiver(receiverActor: ActorRef) => {
      receivers = Option(receivers.get filter (x => x != receiverActor))
      context.unwatch(receiverActor)
      log.info(s"received unsubscribe from $receiverActor , total number of recievers : ${receivers.get.length}")
    }

    case Terminated(actorRef) => {
      log.warning(s"Connection Terminated by $sender")
      self ! UnsubscribeReceiver(sender)
    }

    case "close" =>
      log.info(s"Close connection command by $sender")

    case CommandFailed(w: Write) =>
      // O/S buffer was full
      log.debug(s"Write Command Failed to $sender")

    case CommandFailed(error) => {
      log.info(s"CommandFailed from $sender , cause : ${error.failureMessage}")
      error match
      {
        case connectionError : Connect => {
          log.info(s"connectionError from $sender")
          self ! UnsubscribeReceiver(sender)
        }
      }
    }

    case closedConnection : ConnectionClosed => {
      log.warning(s"Connection was closed by $sender. cause : ${closedConnection.getErrorCause}")
      self ! UnsubscribeReceiver(sender)
    }

    case errorHandling =>
    {
      log.info(s"default errorHandling from $sender")
      self ! UnsubscribeReceiver(sender)
    }
  }
}

/**
  * A sample feeder actor
  * impressions
  * Usage: FeederActor <hostname> <port>
  * <hostname> and <port> describe the AkkaSystem that Spark Sample feeder would start on.
  */
object FeederActor extends Logging with streamingOutConfig {

  //Streaming Server
  val sparkConfig = new SparkConf
  val system = LocalUtils.obj.createActorSystem(actorSystemName, listenHostname, listenPort, sparkConfig,
    LocalSecurityManager.get(sparkConfig))._1
  var feeder: ActorRef = null

  feeder = system.actorOf(Props[FeederActor], actorName)

  log.info(s"Feeder started as:$feeder on $listenHostname:$listenPort as $actorName@$actorSystemName")
}