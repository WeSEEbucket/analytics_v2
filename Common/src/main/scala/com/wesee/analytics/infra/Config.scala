package com.wesee.analytics.infra

import com.typesafe.config.ConfigFactory

trait Config {
  var config = ConfigFactory.load
}

trait streamingOutConfig extends Config {

  private val streamingOutConfig = config.getConfig("streamingOut")

  val listenHostname: String = streamingOutConfig.getString("host")
  val listenPort: Int = streamingOutConfig.getInt("port")
  val actorSystemName: String = streamingOutConfig.getString("actor-system-name")
  val actorName: String = streamingOutConfig.getString("actor-name")
  val connectionProtocol: String = streamingOutConfig.getString("connection-protocol")

}

trait MonitorConfig extends Config {

  val monitorConfig = config.getConfig("monitor")

  private val monitorRemoteActorConfig = monitorConfig.getConfig("remoteActor")

  val aggregatorIntervalInSeconds = monitorRemoteActorConfig.getInt("aggregator-interval-in-seconds")
  val monitorRemoteActorSystem = RemoteSystemSettings(
    monitorRemoteActorConfig.getString("actor-system-name"),
    monitorRemoteActorConfig.getString("host"),
    monitorRemoteActorConfig.getInt("port"),
    monitorRemoteActorConfig.getString("actor-name"),
    monitorRemoteActorConfig.getString(" connection-protocol"))
}

case class RemoteSystemSettings(actorSystemName: String,
                                actorSystemHost: String,
                                actorSystemPort: Int,
                                actorSystemRecieverActor: String,
                                connectionProtocol: String)