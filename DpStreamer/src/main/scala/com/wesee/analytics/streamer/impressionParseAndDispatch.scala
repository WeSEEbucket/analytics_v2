package com.wesee.analytics.streamer


import akka.actor._
import com.wesee.analytics.common.{AggregatorMonitorManager, Dispatch}


class impressionParseAndDispatch extends Actor with ActorLogging {
  val CR = '\r'
  val maxStored = 1000000
  private var buffer: StringBuilder = new StringBuilder()

  {
    buffer.sizeHint(maxStored)
  }

  val receive: Receive = {
    case data: String =>
      var bufferedData = data
      //concat last partial impression to input
      if (!buffer.isEmpty) {
        bufferedData = buffer.append(data).result
        buffer.clear()
      }

      val isCorrupted = bufferedData.last != '\r'

      var impressions = bufferedData.trim.split("\r")

      if (isCorrupted) {
        //save last item for next iteration
        buffer.clear()
        buffer append impressions.lastOption.getOrElse("")
        impressions = impressions take impressions.length - 1
      }

      log.debug(s"${impressions.length} Impressions Received")
      AggregatorMonitorManager.increment("DpStreamerIn_Total_Received_" + self, impressions.length)

      impressions foreach { impression => FeederActor.feeder ! Dispatch(impression) }
    case _ =>
      log.warning(s"Not Supported Input")
  }
}
