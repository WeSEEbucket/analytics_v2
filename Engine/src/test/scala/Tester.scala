import com.wesee.analytics.infra.FilesUtils
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTimeZone, DateTime}

/**
  * Created by raanan on 1/3/16.
  */
object Tester extends App {


  val content: StringBuilder = new StringBuilder

  println("%s/%s/%s".format("/tmp", "/test", DateTimeFormat.forPattern( "YYYY/MM/dd/HH").print(DateTime.now(DateTimeZone.UTC))))

  for (j <- 0 to 23) {
    println("%s/%s/%s".format("/tmp", "/test", DateTimeFormat.forPattern("YYYY/MM/dd/HH").print(DateTime.now(DateTimeZone.UTC).withHourOfDay(j).withDayOfMonth(1).withMonthOfYear(1).withYear(2016))))
  }

  var date = DateTime.now(DateTimeZone.UTC).withDayOfMonth(1).withMonthOfYear(1).withYear(2016)

  /*
  for (i <- 1 to 365) {

    var year = String.format("%02d", int2Integer(date.year().get()))
    var month = String.format("%02d", int2Integer(date.monthOfYear().get()))
    var day = String.format("%02d", int2Integer(date.dayOfMonth().get()))

    for (j <- 0 to 23) {

      var hour = String.format("%02d", int2Integer(j))
      content.append(s"ALTER TABLE impressions_text_partition ADD PARTITION (year=$year, month=$month, day=$day, hour=$hour) location '/home/analytics/data/text/$year/$month/$day/$hour';\r\n")
    }

    date = date.plusDays(1)

  }

  import java.io._

  val pw = new PrintWriter(new File("/home/raanan/git/wesee/Analytics/Deployment/dba/partitions.text" ))
  pw.write(content.toString())
  pw.close

  */

/*
 for (i <- 1 to 365) {

   var year = String.format("%02d", int2Integer(date.year().get()))
   var month = String.format("%02d", int2Integer(date.monthOfYear().get()))
   var day = String.format("%02d", int2Integer(date.dayOfMonth().get()))

   for (j <- 0 to 23) {

     var hour = String.format("%02d", int2Integer(j))
     content.append(s"ALTER TABLE impressions_parquet_partition ADD PARTITION (year=$year, month=$month, day=$day, hour=$hour) location '/home/analytics/data/parquet/$year/$month/$day/$hour';\r\n")
   }

   date = date.plusDays(1)

 }

 import java.io._

 val pw = new PrintWriter(new File("/home/raanan/git/wesee/Analytics/Deployment/dba/partitions.parquet" ))
 pw.write(content.toString())
 pw.close

*/

}