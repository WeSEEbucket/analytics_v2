name := "Analytics"

lazy val commonSettings = Seq(
  version := "1.0",
  organization := "com.wesee",
  scalaVersion := "2.11.7"
)

lazy val Common = (project in file("Common"))
  .settings(commonSettings: _*)

lazy val DpStreamer = (project in file("DpStreamer"))
  .dependsOn(Common)
  .settings(commonSettings: _*)

lazy val Engine = (project in file("Engine"))
  .dependsOn(Common)
  .settings(commonSettings: _*)

assemblyMergeStrategy in assembly := {

  case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
  case PathList("javax", "activation", xs@_*) => MergeStrategy.first
  case PathList("org", "apache", xs@_*) => MergeStrategy.first
  case PathList("org", "joda", xs@_*) => MergeStrategy.first
  case PathList("org", "jboss", xs@_*) => MergeStrategy.first
  case PathList("org", "xerial", xs@_*) => MergeStrategy.first
  case PathList("com", "google", xs@_*) => MergeStrategy.first
  case PathList("xml-apis", "xml-apis", xs@_*) => MergeStrategy.first
  case PathList("com", "esotericsoftware", xs@_*) => MergeStrategy.first
  case PathList("javax", "xml", xs@_*) => MergeStrategy.first
  case PathList("org", "slf4j", xs@_*) => MergeStrategy.first
  case PathList("scala", "reflect", xs@_*) => MergeStrategy.first
  case PathList("log4j", "log4j", xs@_*) => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}