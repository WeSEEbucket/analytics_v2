package com.wesee.analytics.spark.report

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

/*
Schema
|-- version: string (nullable = true)
|-- callerType: string (nullable = true)
|-- Identification: string (nullable = true)
|-- classificationsMap: map (nullable = true)
|    |-- key: integer
|    |-- value: integer (valueContainsNull = true)
|-- classificationsList: array (nullable = true)
|    |-- element: integer (containsNull = true)
|-- authority: string (nullable = true)
|-- client: string (nullable = true)
|-- location: string (nullable = true)
|-- cacheHit: string (nullable = true)
|-- queryParams: string (nullable = true)
|-- time: long (nullable = true)
*/

trait reportCommand {

  protected val sqlContext = SparkUtils.getSQLContext()

  def execute(dataFrame: DataFrame): DataFrame
}

class TopLocation extends reportCommand {

  def execute(dataFrame: DataFrame): DataFrame = {

    import sqlContext.implicits._

    return dataFrame.select("location", "cacheHit").
      groupBy("location").
      count.orderBy("count").
      sort($"count".desc).limit(100)
  }
}

class CountClassification extends reportCommand {

  def execute(dataFrame: DataFrame): DataFrame = {

    import sqlContext.implicits._

    return dataFrame.select("classificationsList").
      explode[Seq[Int], Int]("classificationsList", "classification") { v => v.toList }.select("classification").
      groupBy("classification").
      count.orderBy("count").
      sort($"count".desc)
  }
}

class CountClassificationWithIAB extends reportCommand {

  def execute(dataFrame: DataFrame): DataFrame = {

    import sqlContext.implicits._

    return dataFrame.select("classificationsList").
      explode[Seq[Int], Int]("classificationsList", "classification") { v => v.toList }.select("classification").
      groupBy("classification").
      count.orderBy("count").
      sort($"count".desc)
  }
}

class CountCacheHit extends reportCommand {

  def execute(dataFrame: DataFrame): DataFrame = {

    import sqlContext.implicits._

    return dataFrame.select("cacheHit").
      groupBy("cacheHit").
      count.orderBy("count").
      sort($"count".desc)
  }
}

class CountCacheHitUnique extends reportCommand {

  def execute(dataFrame: DataFrame): DataFrame = {

    import sqlContext.implicits._

    return dataFrame.select("cacheHit", "location").
      groupBy("cacheHit").
      agg(count("cacheHit"), countDistinct("location")).
      orderBy("count(cacheHit)").
      sort($"count(cacheHit)".desc)
  }
}

/*

select cacheHit , count(distinct location), count(*) from impressions_parquet_partition
where year=2016 and month=2 and day=3
group by cacheHit
order by count(*) desc;

c	2941590	86392951
w	5634670	27745140
n	2792373	6755419
e	3196258	5533543
an	1264272	1636463
b	86539	861905
toRedis	3	3

 */

class AllCommand extends reportCommand {

  def execute(dataFrame: DataFrame): DataFrame = {

    return dataFrame
  }
}

object ReportsCommands {

  val mapCommand: Map[String, reportCommand] = Map(
    "TopLocation" -> (new TopLocation),
    "All" -> (new AllCommand),
    "CountCacheHit" -> (new CountCacheHit),
    "CountCacheHitUnique" -> (new CountCacheHitUnique),
    "CountClassification" -> (new CountClassification))

  def getDataFrame(reportName: String, dataFrame: DataFrame): DataFrame = {

    return mapCommand(reportName).execute(dataFrame)
  }
}