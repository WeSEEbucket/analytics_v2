package com.wesee.analytics.streamer

import java.net.InetSocketAddress

import akka.actor.{ActorSystem, Props}
import akka.event.Logging
import com.wesee.analytics.infra.streamingInConfig

object StreamingApp extends App with streamingInConfig {

  lazy val log = Logging.getLogger(system, this)
  implicit val system = ActorSystem(actorSystemName)

  FeederActor

  log.info(s"Starting Analytics Inbound Service. will listen on host:port : $listenHostname:$listenPort")
  val dispatcher = system.actorOf(Props[impressionParseAndDispatch], "Dispatcher")

  //Inbound TCP server
  val endpoint = new InetSocketAddress(listenHostname, listenPort)
  new DataProviderInBoundServer().run(endpoint, dispatcher)
}