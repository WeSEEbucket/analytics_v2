package com.wesee.analytics.streamer;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import com.wesee.analytics.common.AggregatorMonitorManager;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.compression.ZlibCodecFactory;
import io.netty.handler.codec.compression.ZlibWrapper;

class InboundConnectionHandler extends SimpleChannelInboundHandler<ByteBuf> {

    final private String id = this.toString();
    ActorRef dispatcher;

    public InboundConnectionHandler(ActorRef _dispatcher) {
        dispatcher = _dispatcher;
    }

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        byte[] bytes = new byte[msg.readableBytes()];
        msg.readBytes(bytes);
      //  AggregatorMonitorManager.increment("DpStreamerIn_JavaServerIn_Total_Received_" + id, 1L);
        Patterns.ask(dispatcher, new String(bytes), 10);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}

class TcpServerHandlerInitializer extends ChannelInitializer<SocketChannel> {

    ActorRef dispatcher;

    public TcpServerHandlerInitializer(ActorRef _dispatcher) {
        dispatcher = _dispatcher;
    }

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline p = ch.pipeline();
        p.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));
        p.addLast(new InboundConnectionHandler(dispatcher));
    }
}
