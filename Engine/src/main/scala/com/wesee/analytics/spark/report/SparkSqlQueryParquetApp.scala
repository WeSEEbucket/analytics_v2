package com.wesee.analytics.spark.report

import com.wesee.analytics.infra.{FilesUtils, Logging, SparkConfig}
import org.apache.spark.sql.{SQLContext, DataFrame, SaveMode}

/**
arg(0) = report file name
  arg(1) = query { SQL command }
  arg(2) = range { From date to date. format_from  ~  format_to}
  arg(3) = date format { like yyyy/MM/dd }

 **/

object SparkSqlQueryParquetApp extends App with SparkConfig with Logging {

  // Validate input arguments
  (args.length == 4) match {
    case true => None
    case false => log.error("Expecting input arguments: [report name] [SQL command] [from_date~to_data] [date format]")
      log.error(args.length.toString)
      log.error(args.mkString)
      System.exit(1)
  }

  val dataFormat = "parquet"

  val reportName = args(0)

  log.info(s"Started SparkSqlQueryParquetApp for report : $reportName")
  log.info(args.deep.mkString(" "))

  // Get SQL command
  val sqlCommand = args(1)
  log.info(s"SQl Command : $sqlCommand")

  // Get date range & date format
  val dateRange = args(2)
  log.info(s"Report Date Range : $dateRange")

  val dateFormat = args(3)
  log.info(s"Report Date Format : $dateFormat")

  try {

    val impressions = ReportsUtils.getDataFrame(dataFormat, dateRange, dateFormat)

    // Create Spark SQL interface
    impressions.registerTempTable(sparkSchemaName)

    // Load Directories and transform to DataFrames
    val sqlContext = SparkUtils.getSQLContext()

    // Execute the SQL command
    val countQuery = sqlContext.sql(sqlCommand)

    // Collect data and save report as (CSV)
    countQuery.coalesce(reportPartitions).write
      .format(reportFileFormat)
      .option("header", "true")
      .mode(SaveMode.Overwrite)
      .save(FilesUtils.generateReportFileName(reportBaseDirectory,
        reportDirectoryDatePattern,
        reportName,
        reportFileDatePattern))

  } catch {
    case exception: Throwable => log.error(s"Got Throwable execption" + exception ,exception)
  }

  def tryLoad(sqlContext: SQLContext, directory: String): DataFrame = {
    try {
      sqlContext.read.format(dataFormat).load(directory)
    } catch {
      case exception: Throwable => log.error(s"Try load $directory, Got Throwable execption" + exception,exception)
        sqlContext.emptyDataFrame
    }
  }

  log.info(s"Finished SparkSqlQueryParquetApp for report : $reportName")
}