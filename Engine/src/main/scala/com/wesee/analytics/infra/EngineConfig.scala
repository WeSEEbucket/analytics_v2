package com.wesee.analytics.infra

trait SparkConfig extends Config {

  /*             */
  private val sparkConfig = config.getConfig("spark")


  /*             */
  private val streamConfig = sparkConfig.getConfig("stream")

  val sparkStream_BatchDuration: Long = streamConfig.getLong("batch-duration")
  val sparkStream_WindowDuration: Long = streamConfig.getLong("window-duration")
  val sparkStreams_SlideDuration: Long = streamConfig.getLong("slide-duration")
  val sparkStream_NoDataWindowSize: Long = streamConfig.getLong("no-data-window-size")
  val sparkStream_shutDownCommand: String = streamConfig.getString("shutdown-command")
  val receiverActorName: String = streamConfig.getString("receiver-actor-name")

  /*             */
  private val dataConfig = sparkConfig.getConfig("filesystem.data")

  val dataBaseDirectory: String = dataConfig.getString("base-directory")
  val dataDirectoryDatePattern: String = dataConfig.getString("directory-date-pattern")
  val dataPartitions: Int = dataConfig.getInt("numPartitions")
  val dataTextDelimiter: String = dataConfig.getString("textDelimiter")

  /*             */
  private val reportConfig = sparkConfig.getConfig("filesystem.report")

  val reportBaseDirectory: String = reportConfig.getString("base-directory")
  val reportFileFormat: String = reportConfig.getString("file-format")
  val reportDirectoryDatePattern: String = reportConfig.getString("directory-date-pattern")
  val reportFileDatePattern: String = reportConfig.getString("file-date-pattern")
  val reportPartitions: Int = reportConfig.getInt("numPartitions")

  /*             */
  private val sparkSchema = sparkConfig.getConfig("schema")

  val sparkSchemaName: String = sparkSchema.getString("name")
  val sparkSchemaStoreQuery: String = sparkSchema.getString("store-query")

  val sparkMaster: String = sparkConfig.getString("master")

  /*             */
  private val applicationConfig = config.getConfig("application")

  val reportAppName: String = applicationConfig.getString("report-app-name")
}