#!/usr/bin/env bash

SPARK_HOME="/opt/deploy/spark"

ANALYTICES_HOME="/home/analytics"

#SPARK_MASTER=local[*]
SPARK_MASTER=spark://`hostname`:7077

JAR=$ANALYTICES_HOME/bin/Engine.jar

CLASS=com.wesee.analytics.spark.stream.apps.TextSparkStreamReceiver

SPARK_DEPLOY_MODE="client"

JAVA_OPTS="-Dspark.local.dir=$ANALYTICES_HOME/temp
           -Djava.net.preferIPv4Stack=true \
           -Djava.library.path=/usr/lib64/libsnappy.so \
           -Dorg.xerial.snappy.tempdir=/home/analytics/temp"


# To truly enable JMX in AWS and other containerized environments, also need to set
# -Djava.rmi.server.hostname equal to the hostname in that environment.  This is specific
# depending on AWS vs GCE etc.
JMX_PORT=5558
JMX_OPTS=" -Dcom.sun.management.jmxremote=true \
           -Dcom.sun.management.jmxremote.ssl=false \
           -Dcom.sun.management.jmxremote.authenticate=false \
           -Dcom.sun.management.jmxremote.port=$JMX_PORT \
           -Djava.rmi.server.hostname=`hostname` \
           -Dcom.sun.management.jmxremote.rmi.port=$JMX_PORT"

JAVA_LOGS="-Dlog4j.configuration=file:$ANALYTICES_HOME/conf/log4j.sparkStreamText.properties"

JAVA_CONF="-Dconfig.file=$ANALYTICES_HOME/conf/sparkTextApplication.conf"

exec "$SPARK_HOME"/bin/spark-submit \
  --master $SPARK_MASTER \
  --class $CLASS \
  --driver-java-options "$JAVA_OPTS $JMX_OPTS $JAVA_LOGS $JAVA_CONF" \
  --deploy-mode $SPARK_DEPLOY_MODE \
  --total-executor-cores 4 \
  "$JAR" \
  "$@"