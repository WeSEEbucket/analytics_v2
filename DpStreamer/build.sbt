name := "DpStreamer"

version := "1.0"
organization := "com.wesee"
scalaVersion := "2.11.7"

unmanagedJars in Compile += file("../Common/target/scala-2.11/Common.jar")

assemblyJarName in assembly := "DpStreamer.jar"

mainClass in assembly := Some("com.wesee.analytics.streamer.StreamingApp")

libraryDependencies += "io.netty" % "netty-all" % "5.0.0.Alpha2"
libraryDependencies += "io.netty" % "netty" % "3.10.5.Final"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "1.6.0"

libraryDependencies += "com.typesafe" % "config" % "latest.integration"
libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.3.11"
libraryDependencies += "com.typesafe.akka" % "akka-remote_2.11" % "2.3.11"
libraryDependencies += "com.typesafe.akka" % "akka-slf4j_2.11" % "2.3.11"

libraryDependencies += "ch.qos.logback" % "logback-core" % "latest.integration"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "latest.integration"

libraryDependencies += "com.typesafe.play" % "play-json_2.11" % "2.4.4" exclude("com.fasterxml.jackson.core","jackson-databind")

//libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "latest.integration"

libraryDependencies += "com.jcraft" % "jzlib" % "1.1.3"
libraryDependencies += "com.ning" % "compress-lzf" % "1.0.3"
libraryDependencies += "net.jpountz.lz4" % "lz4" % "1.3.0"
libraryDependencies += "com.github.jponge" % "lzma-java" % "1.3"

assemblyMergeStrategy in assembly := {

  case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
  case PathList("javax", "activation", xs@_*) => MergeStrategy.first
  case PathList("org", "apache", xs@_*) => MergeStrategy.first
  case PathList("org", "joda", xs@_*) => MergeStrategy.first
  case PathList("org", "jboss", xs@_*) => MergeStrategy.first
  case PathList("org", "xerial", xs@_*) => MergeStrategy.first
  case PathList("xml-apis", "xml-apis", xs@_*) => MergeStrategy.first
  case PathList("com", "esotericsoftware", xs@_*) => MergeStrategy.first
  case PathList("com", "google", xs@_*) => MergeStrategy.first
  case PathList("com", "typesafe", xs@_*) => MergeStrategy.first
  case PathList("com", "typesafe.akka", xs@_*) => MergeStrategy.first
  case PathList("akka", "pattern", xs@_*) => MergeStrategy.first
  case PathList("javax", "xml", xs@_*) => MergeStrategy.first
  case PathList("org", "slf4j", xs@_*) => MergeStrategy.first
  case PathList("scala", "reflect", xs@_*) => MergeStrategy.first
  case PathList("log4j", "log4j", xs@_*) => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}