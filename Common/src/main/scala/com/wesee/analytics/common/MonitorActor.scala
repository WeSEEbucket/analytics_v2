package com.wesee.analytics.common

import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicLong

import akka.actor._
import com.wesee.analytics.infra.MonitorConfig

import scala.concurrent.duration._

/**
  *
  */
trait Monitor {

  def increment(name: String, value: Long): Unit

  def set(name: String, value: Long): Unit
}

/**
  *
  */
object MonitorManager extends Monitor with MonitorConfig {

  val system = ActorSystem.create("MonitorManager", monitorConfig)
  val proxyActor = system.actorOf(Props[RemoteMonitorActorProxy], "localMonitorActor")

  def increment(name: String, value: Long): Unit = {
    proxyActor ! new KeyValue(name, value.toString, KeyValueOperations.increment.toString)
  }

  def set(name: String, value: Long): Unit = {
    proxyActor ! new KeyValue(name, value.toString, KeyValueOperations.set.toString)
  }
}

/**
  *
  */
object AggregatorMonitorManager extends Monitor with Cancellable with MonitorConfig {

  val system = ActorSystem.create("AggregatorMonitorManager", monitorConfig)
  val proxyActor = system.actorOf(Props[RemoteMonitorActorProxy], "localMonitorActor")
  private val incrementMap = scala.collection.mutable.HashMap.empty[String, AtomicLong]
  private val setMap = scala.collection.mutable.HashMap.empty[String, AtomicLong]

  import scala.concurrent.ExecutionContext.Implicits.global

  system.scheduler.schedule(0.seconds, FiniteDuration.apply(aggregatorIntervalInSeconds, TimeUnit.SECONDS))(scheduledSend())

  def increment(name: String, value: Long): Unit = {

    this.synchronized {
      incrementMap.contains(name) match {
        case true => incrementMap(name).addAndGet(value)
        case false => incrementMap(name) = new AtomicLong(value)
      }
    }
  }

  def set(name: String, value: Long): Unit = {

    this.synchronized {
      setMap(name) = new AtomicLong(value.toLong)
    }
  }

  def scheduledSend(): Unit = {

    this.synchronized {

      incrementMap.foreach(record => proxyActor ! new KeyValue(record._1, record._2.toString, "increment"))
      incrementMap.clear()

      setMap.foreach(record => proxyActor ! new KeyValue(record._1, record._2.toString, "set"))
      setMap.clear()
    }
  }

  final def cancel(): Boolean = {
    true
  }

  override def isCancelled: Boolean = true
}

class RemoteMonitorActorProxy extends Actor with KeyValueGenerator with MonitorConfig {

  val remoteActor = context.actorSelection(s"${monitorRemoteActorSystem.connectionProtocol}://${monitorRemoteActorSystem.actorSystemName}@${monitorRemoteActorSystem.actorSystemHost}:${monitorRemoteActorSystem.actorSystemPort}/${monitorRemoteActorSystem.actorSystemRecieverActor}")

  def receive = {
    case kv: KeyValue => remoteActor ! generate(kv)
  }
}