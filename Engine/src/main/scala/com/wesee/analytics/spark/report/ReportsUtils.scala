package com.wesee.analytics.spark.report

import org.apache.spark.sql.{SQLContext, DataFrame}
import com.wesee.analytics.infra.{FilesUtils, Logging, SparkConfig}


object ReportsUtils extends SparkConfig with Logging {

  // Load Directories and transform to DataFrames
  private val sqlContext = SparkUtils.getSQLContext()

  private def tryLoad(sqlContext: SQLContext, directory: String, dataFormat: String): DataFrame = {
    try {
      sqlContext.read.format(dataFormat).load(directory)
    } catch {
      case exception: Throwable => //log.error(s"Try load $directory, Got Throwable execption" + exception, exception)
        sqlContext.emptyDataFrame
    }
  }

  def getDataFrame(reportDataFormat: String, reportDateRange: String, reportDateFormat: String): DataFrame = {

    try {

      // Get the directories to process for that date range
      val directories: Seq[String] = FilesUtils.getDirectoriesForDateRange(dataBaseDirectory,
        reportDataFormat,
        reportDateRange,
        reportDateFormat)

      log.info(s"Directories : $directories")

      val rddParquet: Seq[DataFrame] = directories.map(x => tryLoad(sqlContext, x, reportDataFormat)).filter(_ != sqlContext.emptyDataFrame)

      // Join DataFrames to one DataFrame
      var impressions: DataFrame = rddParquet.reduce((s1, s2) => s1.unionAll(s2))

      return impressions

    }
    catch {
      case exception: Throwable => log.error(s"Try get ReportDataFrame, Got Throwable execption" + exception, exception)
        return sqlContext.emptyDataFrame
    }
  }
}