package com.wesee.analytics.spark.stream.recievers

import akka.actor._
import com.wesee.analytics.common.{SubscribeReceiver, UnsubscribeReceiver}
import com.wesee.analytics.infra.streamingOutConfig
import org.apache.spark.streaming.receiver.ActorHelper

import scala.concurrent.duration.Duration
import scala.reflect.ClassTag

class AnalyticsReciever[T: ClassTag](urlOfPublisher: String)
  extends Actor with ActorHelper  {

  private val remotePublisher = context.actorSelection(urlOfPublisher)

  log.info(s"urlOfPublisher : $urlOfPublisher")

  override def preStart() = identifyEcho()

  def identifyEcho(): Unit = {
    remotePublisher ! Identify(0)
  }

  def receive = initializing

  def initializing: Receive = {
    case ActorIdentity(0, Some(ackRef)) =>
      ackRef ! SubscribeReceiver(self)
      becomeRunning(ackRef)
    case ReceiveTimeout => identifyEcho()
  }

  def running(echo: ActorRef): Receive = {
    case Terminated(`echo`) =>
      context become initializing
    case msg => store(msg.asInstanceOf[T])
  }

  def becomeRunning(echo: ActorRef): Unit = {
    log.info("Running")
    context setReceiveTimeout Duration.Undefined
    context watch echo
    context become running(echo)
  }

  override def postStop() = remotePublisher ! UnsubscribeReceiver(context.self)
}

object AnalyticsReciever extends streamingOutConfig {

  val urlOfPublisher = s"$connectionProtocol://$actorSystemName@$listenHostname:$listenPort/user/$actorName"

  def props[T: ClassTag](): Props =
    Props(new AnalyticsReciever[T](urlOfPublisher))
}