name := "Common"

version := "1.0"
organization := "com.wesee"
scalaVersion := "2.11.7"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "latest.integration" % "compile"

libraryDependencies += "org.apache.spark" % "spark-streaming_2.11" % "latest.integration" % "compile"

libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.3.11" % "compile"

libraryDependencies += "com.typesafe.play" % "play-json_2.11" % "2.4.4" % "compile" excludeAll(ExclusionRule(organization = "com.fasterxml.jackson.core"))

libraryDependencies += "org.slf4j" % "slf4j-api" % "latest.integration" % "compile"

libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "latest.integration" % "compile"

assemblyJarName in assembly := "Common.jar"

assemblyMergeStrategy in assembly := {

  case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
  case PathList("javax", "activation", xs@_*) => MergeStrategy.first
  case PathList("org", "apache", xs@_*) => MergeStrategy.first
  case PathList("org", "joda", xs@_*) => MergeStrategy.first
  case PathList("org", "jboss", xs@_*) => MergeStrategy.first
  case PathList("org", "xerial", xs@_*) => MergeStrategy.first
  case PathList("com", "google", xs@_*) => MergeStrategy.first
  case PathList("com", "fasterxml", xs@_*) => MergeStrategy.first
  case PathList("com", "typesafe", xs@_*) => MergeStrategy.first
  case PathList("xml-apis", "xml-apis", xs@_*) => MergeStrategy.first
  case PathList("com", "esotericsoftware", xs@_*) => MergeStrategy.first
  case PathList("javax", "xml", xs@_*) => MergeStrategy.first
  case PathList("org", "slf4j", xs@_*) => MergeStrategy.first
  case PathList("scala", "reflect", xs@_*) => MergeStrategy.first
  case PathList("log4j", "log4j", xs@_*) => MergeStrategy.first
  case PathList("commons-beanutils", "commons-beanutils-core", xs@_*) => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}