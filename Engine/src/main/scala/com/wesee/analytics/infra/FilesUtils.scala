package com.wesee.analytics.infra

import java.io.File

import org.joda.time.format.{DateTimeFormat}
import org.joda.time.{DateTime, DateTimeZone}

object FilesUtils extends Logging {

  def generateDataFileName(baseDirectory: String, subDirectory: String, pattern: String): String = {

    "%s/%s/%s".format(baseDirectory, subDirectory, DateTimeFormat.forPattern(pattern).print(DateTime.now(DateTimeZone.UTC)))
  }

  def generateReportFileName(baseDirectory: String, directoryPattern: String, reportName: String, reportNamePattern: String): String = {

    "%s/%s/%s-%s".format(baseDirectory,
      DateTimeFormat.forPattern(directoryPattern).print(DateTime.now(DateTimeZone.UTC)),
      reportName,
      DateTimeFormat.forPattern(reportNamePattern).print(DateTime.now(DateTimeZone.UTC)))
  }

  def getDirectoriesForDateRange(baseDirectory: String, subDirectory: String, range: String, pattern: String): List[String] = {

    // Get Date Range
    val datesRange = getDateRange(range)

    // Get all directories in range
    val dtf = DateTimeFormat.forPattern(pattern)

    var fromDate = dtf.parseLocalDate(datesRange(0))

    val toDate = dtf.parseLocalDate(datesRange(1))

    var directories: List[String] = List[String]()

    while (fromDate != toDate.plusDays(1)) {

      val path = "%s/%s/%s".format(baseDirectory, subDirectory, dtf.print(fromDate))

      log.debug("Checking directory path : " + path)

      new File(path).exists() match {

        case true =>  directories = directories ++ new File(path).listFiles().toList.map( x => x.toString)

        case _ => log.warn("No directories for date " + fromDate)
      }

      fromDate = fromDate.plusDays(1)
    }

    return directories
  }

  private def getDateRange(range: String): Array[String] = {

    val datesRange = range.split("~")

    var dateRange: Array[String] = new Array[String](2)

    datesRange.length match {

      case 0 => log.error("expecting date, got " + datesRange.deep.mkString(","))
        throw new Exception("expecting date, got " + datesRange.deep.mkString)

      case 1 => dateRange(0) = datesRange(0)
        dateRange(1) = datesRange(0)

      case 2 => dateRange(0) = datesRange(0)
        dateRange(1) = datesRange(1)

      case _ => log.error("expecting fromDate ~ toDate, got " + datesRange.deep.mkString)
        throw new Exception("expecting fromDate ~ toDate, got " + datesRange.deep.mkString)
    }

    return dateRange
  }
}