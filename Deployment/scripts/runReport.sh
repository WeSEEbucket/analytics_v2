#!/usr/bin/env bash

ANALYTICES_SCRIPTS_HOME="/home/analytics/sbin"

REPORT_NAME="ALL"
REPORT_DATE=`date  -d "1 days ago" '+%Y/%m/%d'`
FOLDER_FORMAT="YYYY/MM/DD"
REPORT_QUERY="select * from impressions"

$ANALYTICES_SCRIPTS_HOME/startSparkJobParquetQuery.sh "$REPORT_NAME" "$REPORT_QUERY" "$REPORT_DATE~$REPORT_DATE" "$FOLDER_FORMAT"