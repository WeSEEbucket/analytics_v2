package com.wesee.analytics.spark.stream.apps

import akka.actor.Props
import com.wesee.analytics.common._
import com.wesee.analytics.infra._
import com.wesee.analytics.spark.stream.recievers.ImpressionActorReceiver
import org.apache.spark.SparkConf
import org.apache.spark.sql.{SQLContext, SaveMode}
import org.apache.spark.streaming.{Seconds, StreamingContext}


object ParquetSparkStreamReceiver extends App with SparkConfig with Logging {

  log.info(s"Started ParquetSparkStreamReceiver")
  log.info(s"Data directory date pattern : '$dataDirectoryDatePattern'")

  var noDataContinuesCount = 0
  val outputType = "parquet"

  val sparkConf = new SparkConf()
    .setAppName(outputType)
    .setMaster(sparkMaster)
    .set("spark.akka.logLifecycleEvents", "false") // not redirect undelivered events to dead letter channel

  // Create the context and set the batch size
  val ssc = new StreamingContext(sparkConf, Seconds(sparkStream_BatchDuration))

  val impressions = ssc.actorStream[Impression](
    Props(new ImpressionActorReceiver[Impression]()), receiverActorName)

  impressions.foreachRDD { rdd =>

    try {

      MonitorManager.increment("Parquet_Total_Rounds", 1L)

      // Get the singleton instance of SQLContext
      val sqlContext = SQLContext.getOrCreate(rdd.sparkContext)
      import sqlContext.implicits._

      // Convert RDD[Impression] to DataFrame[Impression]
      val impressionsDataFrame = rdd.toDF()

      // Register as table
      impressionsDataFrame.registerTempTable(sparkSchemaName)

      impressionsDataFrame.count() match {

        case 0L => log.info("RDD is empty, not writing output")

          MonitorManager.set("Parquet_Last_WrittenRecords", 0L)
          MonitorManager.increment("Parquet_Total_WrittenRecords", 0L)
          noDataContinuesCount = noDataContinuesCount + 1

          if(noDataContinuesCount >= sparkStream_NoDataWindowSize) {
            log.warn(s"No data was received during the last $noDataContinuesCount. committing suicide")
            Utils.Stop(sparkStream_shutDownCommand)
          }

        case counter: Long => log.info(s"Number of Records  $counter")
          impressionsDataFrame.coalesce(dataPartitions).write.mode(SaveMode.Append).parquet(FilesUtils.generateDataFileName(
            dataBaseDirectory,
            outputType,
            dataDirectoryDatePattern
          ))
          MonitorManager.set("Parquet_Last_WrittenRecords", counter)
          MonitorManager.increment("Parquet_Total_WrittenRecords", counter)
          noDataContinuesCount = 0
      }

    } catch {
      case exception: Throwable => log.error(s"Got Throwable execption" + exception, exception)
        MonitorManager.increment(ParquetSparkStreamReceiver + "_Total_Exceptions", 1L)
    }
  }

  ssc.start()
  ssc.awaitTermination()

  MonitorManager.set("Parquet_Last_WrittenRecords", 0L)

  log.info(s"Terminated ParquetSparkStreamReceiver")
}