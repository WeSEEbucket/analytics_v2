package com.wesee.analytics.spark.stream.recievers

import akka.actor.Actor
import com.wesee.analytics.common.{SubscribeReceiver, UnsubscribeReceiver}
import com.wesee.analytics.infra.streamingOutConfig
import org.apache.spark.streaming.receiver.ActorHelper

import scala.reflect.ClassTag

/**
  * A sample actor as receiver, is also simplest. This receiver actor
  * goes and subscribe to a typical publisher/feeder actor and receives
  * data.
  *
  */
class SampleActorReceiver[T: ClassTag]()
  extends Actor with ActorHelper with streamingOutConfig {

  private val urlOfPublisher = s"$connectionProtocol://$actorSystemName@$listenHostname:$listenPort/user/$actorName"

  log.info(s"Streamer url Of publisher : $urlOfPublisher")

  private val remotePublisher = context.actorSelection(urlOfPublisher)

  override def preStart(): Unit = remotePublisher ! SubscribeReceiver(context.self)

  def receive: PartialFunction[Any, Unit] = {
    case msg : T => store(msg.asInstanceOf[T])
  }

  override def postStop(): Unit = remotePublisher ! UnsubscribeReceiver(context.self)

  // Todo Add reconnect case handling
}