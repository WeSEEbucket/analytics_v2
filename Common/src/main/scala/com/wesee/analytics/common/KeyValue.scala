package com.wesee.analytics.common

import play.api.libs.functional.syntax._
import play.api.libs.json._


/*  JSON Handlers  */

case class KeyValue(key: String,
                    value: String,
                    operation: String)

object KeyValue {
  // This informs Play how to convert a json value into a KeyValue object
  implicit val reads: Reads[KeyValue] = (
    (JsPath \ "key").read[String] and
      (JsPath \ "value").read[String] and
      (JsPath \ "operation").read[String]) (KeyValue.apply _)

  // This informs Play how to convert a KeyValue pbject into a json value
  implicit val writes: Writes[KeyValue] = (
    (JsPath \ "key").write[String] and
      (JsPath \ "value").write[String] and
      (JsPath \ "operation").write[String]) (unlift(KeyValue.unapply))
}


object KeyValueOperations extends Enumeration {
  type KeyValueOperations = Value
  val increment, set, decrement = Value
}

trait KeyValueGenerator {

  def set(key: String, value: String): String = {
    generate(new KeyValue(key, value, KeyValueOperations.set.toString))
  }

  def increment(key: String, value: String): String = {
    generate(new KeyValue(key, value, KeyValueOperations.increment.toString))
  }

  def generate(kv: KeyValue): String = {
    Json.toJson(kv).toString()
  }
}