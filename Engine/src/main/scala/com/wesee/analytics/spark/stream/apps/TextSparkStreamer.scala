package com.wesee.analytics.spark.stream.apps

import akka.actor.Props
import com.wesee.analytics.common._
import com.wesee.analytics.infra.{FilesUtils, Logging, SparkConfig}
import com.wesee.analytics.spark.stream.recievers.SampleActorReceiver
import org.apache.spark.SparkConf
import org.apache.spark.sql.{SQLContext, SaveMode}
import org.apache.spark.streaming.{Seconds, StreamingContext}


object TextSparkStreamReceiver extends App with SparkConfig with Logging {

  log.info(s"Started TextSparkStreamReceiver")
  log.info(s"Data directory date pattern : '$dataDirectoryDatePattern'")

  val outputType = "text"

  val sparkConf = new SparkConf()
    .setAppName(outputType)
    .setMaster(sparkMaster)
    .set("spark.akka.logLifecycleEvents", "false") // not redirect undelivered events to dead letter channel

  // Create the context and set the batch size
  val ssc = new StreamingContext(sparkConf, Seconds(sparkStream_BatchDuration))

  val impressions = ssc.actorStream[String](
    Props(new SampleActorReceiver[String]()), receiverActorName)

  impressions.foreachRDD { rdd =>

    try {

      MonitorManager.increment("Text_Total_Rounds", 1L)

      // Get the singleton instance of SQLContext
      val sqlContext = SQLContext.getOrCreate(rdd.sparkContext)
      import sqlContext.implicits._

      // Convert RDD[Impression] to DataFrame[Impression]
      val impressionsDataFrame = rdd.toDF()

      // Register as table
      impressionsDataFrame.registerTempTable(sparkSchemaName)

      impressionsDataFrame.count() match {

        case 0L => log.info("RDD is empty, not writing output")
          MonitorManager.set("Text_Last_WrittenRecords", 0L)
          MonitorManager.increment("Text_Total_WrittenRecords", 0L)

        case counter: Long => log.info(s"Number of Records  $counter")
          impressionsDataFrame.coalesce(dataPartitions).write.mode(SaveMode.Append).text(FilesUtils.generateDataFileName(
            dataBaseDirectory,
            outputType,
            dataDirectoryDatePattern
          ))
          MonitorManager.set("Text_Last_WrittenRecords", counter)
          MonitorManager.increment("Text_Total_WrittenRecords", counter)
      }

    } catch {
      case exception: Throwable => log.error(s"Got Throwable execption" + exception + exception.printStackTrace())
        MonitorManager.increment(TextSparkStreamReceiver + "_Total_Exceptions", 1L)
    }
  }

  ssc.start()
  ssc.awaitTermination()

  MonitorManager.set("Text_Last_WrittenRecords", 0L)

  log.info(s"Terminated TextSparkStreamReceiver")
}