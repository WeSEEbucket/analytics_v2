#!/usr/bin/env bash

REPORT_HOME="/home/analytics/report/"
REPORT_DATE_FORMAT_DIRECTORY=`date  -d "1 days ago" '+%Y/%m/%d'`
REPORT_NAME="ALL"

from="SG73 Report System"
to="raanan.nitzan@wesee.com"
subject="Daily Report"
body="This is the body of our email."

declare -a attachments

for dir in $REPORT_HOME$REPORT_DATE_FORMAT_DIRECTORY/*
do
    for r in $dir/*
    do
        if [[ $r == *"part"* ]]
            then attachments+=($r)
        fi
    done
done

declare -a attargs
for att in "${attachments[@]}"; do
  attargs+=( "-a"  "$att" )
  echo "$att"
done

echo $attargs

mail -s "$subject" "${attargs[@]}" "$to" <<< "$body"


