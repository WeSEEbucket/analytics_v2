#!/usr/bin/env bash

ANALYTICES_HOME="/home/analytics"

# To truly enable JMX in AWS and other containerized environments, also need to set
# -Djava.rmi.server.hostname equal to the hostname in that environment.  This is specific
# depending on AWS vs GCE etc.
JMX_PORT=5556
JMX_OPTS=" -Dcom.sun.management.jmxremote=true \
           -Dcom.sun.management.jmxremote.ssl=false \
           -Dcom.sun.management.jmxremote.authenticate=false \
           -Dcom.sun.management.jmxremote.port=$JMX_PORT \
           -Djava.rmi.server.hostname=`hostname` \
           -Dcom.sun.management.jmxremote.rmi.port=$JMX_PORT"

JAVA_OPTS="-Djava.net.preferIPv4Stack=true -Djava.io.tmpdir=$ANALYTICES_HOME"

JAVA_LOGS="-Dlog4j.configuration=file:$ANALYTICES_HOME/conf/log4j.dpStreamer.properties"

JAVA_CONF="-Dconfig.file=$ANALYTICES_HOME/conf/streamerApplication.conf"

JAR="$ANALYTICES_HOME/bin/DpStreamer.jar"

exec java $JAVA_LOGS $JAVA_CONF $JAVA_OPTS $JMX_OPTS -jar $JAR